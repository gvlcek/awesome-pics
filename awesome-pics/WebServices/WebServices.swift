//
//  WebServices.swift
//  awesome-pics
//
//  Created by Guadalupe Vlcek on 29/03/2020.
//  Copyright © 2020 gvlcek. All rights reserved.
//

import Alamofire
import Foundation

class WebService: NSObject {
    
    // MARK:- Albums

    static func fetchAlbums(completionHandler: @escaping ([Album]) -> ()) {
        let BASE_URL = "https://jsonplaceholder.typicode.com/albums"
        
        AF.request(BASE_URL).responseJSON { (response) in
            switch response.result {
            case .success:
                guard let jsonData = response.data else { return }
                do {
                    let albums = try JSONDecoder().decode(Array<Album>.self, from: jsonData)
                    completionHandler(albums)
                } catch let error {
                    print(error)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    // MARK:- Photos
    
    static func fetchPhotos(albumId: Int, completionHandler: @escaping ([Photo]) -> ()) {
        let BASE_URL = "https://jsonplaceholder.typicode.com/photos?albumId=\(albumId.description)"
        
        AF.request(BASE_URL).responseJSON { (response) in
            switch response.result {
            case .success:
                guard let jsonData = response.data else { return }
                do {
                    let photos = try JSONDecoder().decode(Array<Photo>.self, from: jsonData)
                    completionHandler(photos)
                } catch let error {
                    print(error)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    // MARK:- User
    
    static func fetchUser(userId: Int, completionHandler: @escaping ([User]) -> ()) {
        let BASE_URL = "https://jsonplaceholder.typicode.com/users?id=\(userId.description)"
        
        AF.request(BASE_URL).responseJSON { (response) in
            switch response.result {
            case .success:
                guard let jsonData = response.data else { return }
                do {
                    let user = try JSONDecoder().decode(Array<User>.self, from: jsonData)
                    completionHandler(user)
                } catch let error {
                    print(error)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}
