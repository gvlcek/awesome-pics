//
//  Album.swift
//  awesome-pics
//
//  Created by Guadalupe Vlcek on 29/03/2020.
//  Copyright © 2020 gvlcek. All rights reserved.
//

import Foundation

struct Album: Decodable {
    let id: Int!
    let userId: Int!
    let title: String!
}
