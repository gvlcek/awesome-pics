//
//  User.swift
//  awesome-pics
//
//  Created by Guadalupe Vlcek on 29/03/2020.
//  Copyright © 2020 gvlcek. All rights reserved.
//

import Foundation

struct User: Decodable {
    let email: String!
    let id: Int!
    let name: String!
    let username: String!
    let website: String!
}
