//
//  FavoritePhoto.swift
//  awesome-pics
//
//  Created by Guadalupe Vlcek on 30/03/2020.
//  Copyright © 2020 gvlcek. All rights reserved.
//

import UIKit
import RealmSwift

class FavoritePhoto: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var url: String = ""
}
