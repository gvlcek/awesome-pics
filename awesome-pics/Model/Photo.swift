//
//  Photo.swift
//  awesome-pics
//
//  Created by Guadalupe Vlcek on 29/03/2020.
//  Copyright © 2020 gvlcek. All rights reserved.
//

import Foundation

struct Photo: Decodable {
    let albumId: Int!
    let id: Int!
    let title: String!
    let url: String!
    let thumbnailUrl: String!
}
