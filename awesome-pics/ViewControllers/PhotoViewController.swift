//
//  PhotoViewController.swift
//  awesome-pics
//
//  Created by Guadalupe Vlcek on 29/03/2020.
//  Copyright © 2020 gvlcek. All rights reserved.
//

import Alamofire
import AlamofireImage
import UIKit

class PhotoViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, PhotoDetailCollectionViewCellDelegate {
    var defaultIndex: IndexPath!
    var user: User!
    var photos: [Photo]!
    
    @IBOutlet private weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // nav bar
        navigationItem.title = user.name.uppercased()
        
        //back button setup
        let backButton = UIBarButtonItem(image: UIImage(named: "icn_back"), style: .plain, target: self, action: #selector(back))
        backButton.tintColor = .awesome_grey
        navigationItem.leftBarButtonItem = backButton
        
        //scroll to item
        collectionView.layoutIfNeeded()
        collectionView.scrollToItem(at: defaultIndex, at: .top, animated: false)
    }

    @objc private func back(sender: AnyObject) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK:- PhotoDetailCollectionViewCellDelegate
    
    func userDidSharePicture(url: String) {
        let activityVC = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        present(activityVC, animated: true, completion: nil)
    }
    
    // MARK:- UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoDetailCollectionViewCell.identifier, for: indexPath) as! PhotoDetailCollectionViewCell
        if let photo = photos?[indexPath.row] {
            cell.delegate = self
            cell.populate(photo: photo)
        }
        return cell
    }
    
    // MARK:- UICollectionViewDelegateFlowLayout

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width + 100)
    }
}
