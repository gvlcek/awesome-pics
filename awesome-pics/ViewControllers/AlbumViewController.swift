//
//  AlbumViewController.swift
//  awesome-pics
//
//  Created by Guadalupe Vlcek on 29/03/2020.
//  Copyright © 2020 gvlcek. All rights reserved.
//

import UIKit

class AlbumViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    private var photos: [Photo]?
    private var selectedIndex: IndexPath?
    private var user: User?
    var album: Album!
    
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet private weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // nav bar
        navigationItem.title = album.title.uppercased()
        
        // initial data
        updateData()
        
        //back button setup
        let backButton = UIBarButtonItem(image: UIImage(named: "icn_back"), style: .plain, target: self, action: #selector(back))
        backButton.tintColor = .awesome_grey
        navigationItem.leftBarButtonItem = backButton
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Segues.toPhoto {
            let vc = segue.destination as! PhotoViewController
            vc.user = user
            vc.photos = photos
            vc.defaultIndex = selectedIndex
        }
    }
    
    @objc private func back(sender: AnyObject) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK:- Class methods
    
    private func updateData() {
        activityIndicatorView.startAnimating()
        
        WebService.fetchUser(userId: album.userId, completionHandler: { [weak self] result in
            self?.user = result.first
            DispatchQueue.main.async {
                self?.collectionView.reloadData()
            }
        })
      
        WebService.fetchPhotos(albumId: album.id, completionHandler: { [weak self] result in
            self?.photos = result
            DispatchQueue.main.async {
                self?.activityIndicatorView.stopAnimating()
                self?.collectionView.reloadData()
            }
        })
    }
    
    // MARK:- UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCollectionViewCell.identifier, for: indexPath) as! PhotoCollectionViewCell
        if let photo = photos?[indexPath.row] {
            cell.populate(photo: photo)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: HeaderCollectionReusableView.identifier, for: indexPath) as! HeaderCollectionReusableView
        if let user = user {
            reusableView.populate(user: user)
        }
        return reusableView
    }
    
    // MARK:- UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath
        performSegue(withIdentifier: Constants.Segues.toPhoto, sender: nil)
    }
    
    // MARK:- UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width, height: Constants.Layout.Album.height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: Constants.Layout.Album.cellSize, height: Constants.Layout.Album.cellSize)
    }
}
