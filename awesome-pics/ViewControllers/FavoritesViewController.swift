//
//  FavoritesViewController.swift
//  awesome-pics
//
//  Created by Guadalupe Vlcek on 30/03/2020.
//  Copyright © 2020 gvlcek. All rights reserved.
//

import RealmSwift
import UIKit

class FavoritesViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    private var favorites: [FavoritePhoto]?

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // nav bar
        navigationItem.title = NSLocalizedString("favorites_title", comment: "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //get data saved
        fetchFavorites()
    }
    
    func fetchFavorites() {
        let realm = try! Realm()
        let favs = realm.objects(FavoritePhoto.self)
        favorites = favs.compactMap({ $0 })
        collectionView.reloadData()
    }
    
    // MARK:- UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return favorites?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCollectionViewCell.identifier, for: indexPath) as! PhotoCollectionViewCell
        if let favorite = favorites?[indexPath.item] {
            cell.populate(favorite: favorite)
        }
        return cell
    }
    
    // MARK:- UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: Constants.Layout.Album.cellSize, height: Constants.Layout.Album.cellSize)
    }
}
