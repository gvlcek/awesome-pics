//
//  ViewController.swift
//  awesome-pics
//
//  Created by Guadalupe Vlcek on 29/03/2020.
//  Copyright © 2020 gvlcek. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDataSource, HomeAlbumTableViewCellDelegate {
    private var albums: [Album]?
    private var selectedAlbum: Album?
    
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet private weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // initial data
        updateData()
        
        // header imageview
        setTitleView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Segues.toAlbum {
            let vc = segue.destination as! AlbumViewController
            vc.album = selectedAlbum
        }
    }
    
    // MARK:- Class methods
    
    private func updateData() {
        activityIndicatorView.startAnimating()
        
        WebService.fetchAlbums(completionHandler: { [weak self] result in
            self?.albums = result
            DispatchQueue.main.async {
                self?.activityIndicatorView.stopAnimating()
                self?.tableView.reloadData()
            }
        })
    }
    
    func setTitleView() {
        
        if let navigationController = self.navigationController {
            let titleView = UIView(frame: CGRect(x: 0,
                                                 y: 0,
                                                 width: 140,
                                                 height: navigationController.navigationBar.frame.size.height * 2/3))
            let titleImageView = UIImageView(image: #imageLiteral(resourceName: "awesome-pics_logo"))
            titleImageView.contentMode = UIView.ContentMode.scaleAspectFit
            titleImageView.frame = CGRect(x:0,
                                          y:0,
                                          width:titleView.frame.width,
                                          height:titleView.frame.height)
            titleView.addSubview(titleImageView)
            navigationItem.titleView = titleView
        }
    }
    
    // MARK:- HomeAlbumTableViewCellDelegate
    
    func userDidSelectedCell(album: Album) {
        selectedAlbum = album
        performSegue(withIdentifier: Constants.Segues.toAlbum, sender: nil)
    }
    
    // MARK:- UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albums?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: HomeAlbumTableViewCell.identifier) as! HomeAlbumTableViewCell
        if let album = albums?[indexPath.row] {
            cell.delegate = self
            cell.populate(album: album)
        }
        return cell
    }
}

