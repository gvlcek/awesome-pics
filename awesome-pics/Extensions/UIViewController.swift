//
//  UIViewController.swift
//  awesome-pics
//
//  Created by Guadalupe Vlcek on 29/03/2020.
//  Copyright © 2020 gvlcek. All rights reserved.
//

import UIKit

extension UIViewController {
    static var identifier: String { return String(describing: self) }
}
