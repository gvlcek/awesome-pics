//
//  UIColor.swift
//  awesome-pics
//
//  Created by Guadalupe Vlcek on 30/03/2020.
//  Copyright © 2020 gvlcek. All rights reserved.
//

import UIKit

extension UIColor {
    // Color Palette
    static var awesome_grey = UIColor(hex: 0x333333)
    
    // Create a UIColor from RGB
    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
        self.init(
            red: CGFloat(red) / 255.0,
            green: CGFloat(green) / 255.0,
            blue: CGFloat(blue) / 255.0,
            alpha: a
        )
    }
    
    // Create a UIColor from a hex value
    convenience init(hex: Int, a: CGFloat = 1.0) {
        self.init(
            red: (hex >> 16) & 0xFF,
            green: (hex >> 8) & 0xFF,
            blue: hex & 0xFF,
            a: a
        )
    }
}
