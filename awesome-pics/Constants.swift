//
//  Constants.swift
//  awesome-pics
//
//  Created by Guadalupe Vlcek on 30/03/2020.
//  Copyright © 2020 gvlcek. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    struct Segues {
        static let toPhoto: String = "TO_PHOTO"
        static let toAlbum: String = "TO_ALBUM"
    }
    struct Layout {
        struct Album {
            static let height = CGFloat(110)
            static let cellSize = UIScreen.main.bounds.size.width / 3
        }
        struct View {
            static let cornerRadius = CGFloat(5.0)
        }
        struct HomeCell {
            static let borderWidth = CGFloat(1.0)
        }
    }
}
