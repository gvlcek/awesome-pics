//
//  PhotoCollectionViewCell.swift
//  awesome-pics
//
//  Created by Guadalupe Vlcek on 29/03/2020.
//  Copyright © 2020 gvlcek. All rights reserved.
//

import Alamofire
import AlamofireImage
import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var photoImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // placeholder
        photoImageView?.image = UIImage(named: "img_placeholder")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        // reset with placeholder
        photoImageView?.image = UIImage(named: "img_placeholder")
    }
    
    // MARK:- Class methods
    
    func populate(photo: Photo) {
        if let imageURL = URL(string: photo.thumbnailUrl) {
            photoImageView.af.setImage(withURL: imageURL)
        }
    }
    
    func populate(favorite: FavoritePhoto) {
        if let imageURL = URL(string: favorite.url) {
            photoImageView.af.setImage(withURL: imageURL)
        }
    }
}
