//
//  PhotoDetailCollectionViewCell.swift
//  awesome-pics
//
//  Created by Guadalupe Vlcek on 30/03/2020.
//  Copyright © 2020 gvlcek. All rights reserved.
//

import RealmSwift
import UIKit

protocol PhotoDetailCollectionViewCellDelegate: NSObjectProtocol {
    func userDidSharePicture(url: String)
}

class PhotoDetailCollectionViewCell: UICollectionViewCell {
    weak var delegate: PhotoDetailCollectionViewCellDelegate?
    private var photo: Photo!
    
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var favoriteButton: UIButton!
    @IBOutlet private weak var photoImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // placeholder
        photoImageView?.image = UIImage(named: "img_placeholder")
                
        //corner radius
        avatarImageView.layer.cornerRadius = avatarImageView.bounds.height / 2
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        favoriteButton.setImage(UIImage(named: "icn_favorite_border"), for: .normal)
    }

    @IBAction func shareAction(_ sender: UIButton) {
        delegate?.userDidSharePicture(url: photo.url)
    }
    
    @IBAction func favoriteAction(_ sender: UIButton) {
        let realm = try! Realm()
        let favs = realm.objects(FavoritePhoto.self)
        if let currentPhoto = favs.first(where: { $0.id == photo.id }) {
            try! realm.write {
                realm.delete(currentPhoto)
                favoriteButton.setImage(UIImage(named: "icn_favorite_border"), for: .normal)
            }
        } else {
            try! realm.write {
                let favoritePhoto = FavoritePhoto()
                favoritePhoto.url = photo.thumbnailUrl
                favoritePhoto.id = photo.id
                realm.add(favoritePhoto)
                favoriteButton.setImage(UIImage(named: "icn_favorite"), for: .normal)
            }
        }
    }
    
    // MARK:- Class methods
    
    func populate(photo: Photo) {
        self.photo = photo
        titleLabel.text = photo.title

        if let image = photo.url, let imageURL = URL(string: image) {
            photoImageView.af.setImage(withURL: imageURL)
        }
        
        let realm = try! Realm()
        let favs = realm.objects(FavoritePhoto.self)
        if (favs.first(where: { $0.id == photo.id }) != nil) {
            favoriteButton.setImage(UIImage(named: "icn_favorite"), for: .normal)
        }
    }
}
