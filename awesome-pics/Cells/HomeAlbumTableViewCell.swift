//
//  HomeAlbumTableViewCell.swift
//  awesome-pics
//
//  Created by Guadalupe Vlcek on 29/03/2020.
//  Copyright © 2020 gvlcek. All rights reserved.
//

import UIKit

protocol HomeAlbumTableViewCellDelegate: NSObjectProtocol {
    func userDidSelectedCell(album: Album)
}

class HomeAlbumTableViewCell: UITableViewCell {
    weak var delegate: HomeAlbumTableViewCellDelegate?
    
    private var album: Album?
    
    @IBOutlet private weak var cardView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cardView.layer.cornerRadius = Constants.Layout.View.cornerRadius
        cardView.layer.borderColor = UIColor.awesome_grey.cgColor
        cardView.layer.borderWidth = Constants.Layout.HomeCell.borderWidth
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tappedAction))
        self.addGestureRecognizer(tapGesture)
    }

    // MARK:- Class methods
    
    @objc private func tappedAction() {
        guard let album = album else { return }
        delegate?.userDidSelectedCell(album: album)
    }
    
    func populate(album: Album) {
        self.album = album
        titleLabel.text = album.title
    }
}
