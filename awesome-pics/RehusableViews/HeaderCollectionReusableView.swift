//
//  HeaderCollectionReusableView.swift
//  awesome-pics
//
//  Created by Guadalupe Vlcek on 29/03/2020.
//  Copyright © 2020 gvlcek. All rights reserved.
//

import UIKit

class HeaderCollectionReusableView: UICollectionReusableView {
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var emailLabel: UILabel!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var usernameLabel: UILabel!
    @IBOutlet private weak var websiteLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        avatarImageView.layer.cornerRadius = avatarImageView.bounds.height / 2
    }
    
    // MARK:- Class methods
    
    func populate(user: User) {
        nameLabel.text = NSLocalizedString("header_name", comment: "") + user.name
        emailLabel.text = NSLocalizedString("header_email", comment: "") + user.email
        usernameLabel.text = NSLocalizedString("header_username", comment: "") + user.username
        websiteLabel.text = NSLocalizedString("header_website", comment: "") + user.website
    }
}
